import sqlite3


class weather_db:
    def __init__(self,**kwargs):
        """
            db=weather_b=db([table=''])
            constructor method
                table is for CRUD methods
        """
        self._table=kwargs.get('table','')

    def set_table(self, tablename):
        self._table = tablename

    def row_query(self, sql, params=()):
        """returns a single row"""
        cur=self._db.execute(sql, params)
        return cur.fetchone()
    
    def sql_query(self, sql, params=()):
        """ returns one generator row in iteration"""
        cur=self._db.execute(sql, params)
        for p in cur:
            yield p

    def value_query(self, sql, params=()):
        """return a single value from params"""
        cur=self._db.execute(sql, params)
        return cur.fetchone()[0]

    def do_sql(self, sql, params=()):
        """
            db.do_sql for non-select queries
            """
        self._db.execute(sql, params)
        self._db.commit()

    def commit(self):
        self._db.commit()


# CRUD functions/methods

    # insert into table

    def insert_2db(self, conn, city_weather_data):

        sql = '''INSERT INTO City_weather_Data(id,city_name,clouds,tempreture,
        wind_speed,humanidy,country) VALUE(?,?,?,?,?,?,?)'''
        cur = conn.cursor()
        cur.execute(sql, city_weather_data)
        conn.commit()
        return cur.lastrowid()

    def remove_row(self, rowid):
         sql_query = f'DELETE FROM {self._table} WHERE id = ?'
         self._db.execute(sql_query, [rowid])
         self._db.commit()

    def update(self, rowid, row):
        """
            update a table row usind row id
        """
        sklist = sorted(row.keys())
        rowvalues = [row[s] for s in sklist]  # ordered values by keys

        for k, v in sklist:  # don't update id
            if v == 'id':
                del sklist[k]
                del rowvalues[k]

        sql = 'UPDATE {} SET {} WHERE id = ?'.format(
            self._table,
            ',  '.join(map(lambda t: '{} = ?'.format(t), sklist))
        )
        c=self._db.execute(sql, rowvalues + [rowid])
        self._db.commit()
        return c.lastrowid()
    
    def count_query(self):
        """
            returns number of rows in the table
        """
        query = f'SELECT COUNT(*) FROM {self._table}'
        c = self._db.execute(query)
        return c.fetchone()[0]
