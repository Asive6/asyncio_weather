import datetime
import aiohttp
import asyncio
import json
import time
import os
import sqlite3
from sqlite3 import Error
from aiohttp import ClientSession
from dbCrud_class import weather_db
from pip._vendor.distlib.compat import raw_input


user_api_key = os.environ.get("open_weather_api_key") #access api key through window's environment
city_name = raw_input("Enter City Name: ")# get city name through user
sqlite_db = weather_db()


# function to scrap url api and return a json object file
async def get_weather_forecast_async(api_key, city, session):
    """Get weather data asynchronously"""
    url = "https://api.openweathermap.org/data/2.5/weather?q=" + city + "&appid="
    try:
        async with session.get(url) as response:
            response_json = await response.json()
    except Exception as e:
        print("An error occurred: {}".format(e))
    return response_json


# get data our interest from the json file
def extract_data_from_response(response):
    print(response)
    clouds = response["weather"][0]["description"]
    temperature = response["main"]["temp"]
    wind_speed = response["wind"]["speed"]
    humidity = response["main"]["humidity"]
    city = response["name"]
    country = response["sys"]["country"]
    return {"city": city, "country": country, "description": clouds, "temp": temperature, "wind": wind_speed,
            "humidity": humidity}


# event loop Update data every minute
async def periodic_fetch(conn, api_key, city, session):
    """event loop Update data every minute and
        inserting return data into our db using our crud function insert from crud class
    """
    while True:
        data = await get_weather_forecast_async(api_key, city, session)  # call function that is to be refreshed
        row_2badd = (extract_data_from_response(data))
        sqlite_db.insert_2db(conn, row_2badd)
        await asyncio.sleep(60)

#function and loop to display city extraction from db table
def display_weather(db_connection):
    cur=db_connection.cusor()
    cur.execute("SELECT * FROM City_Weather_Data")
    rows=cur.fetchall()
    for row in rows:
        date_time = datetime.now().strftime("%d %b %Y | %I:%M:%S %p")
        print("-------------------------------------------------------------")
        print("Weather forecast for - {},{} || {}".format(rows["city"], rows["country"], date_time))
        print("-------------------------------------------------------------")
        print("Current temperature is: {:.2f} deg C".format(rows["temp"]))
        print("Current weather desc  :", rows["description"])
        print("Current Humidity      :", rows["humidity"], '%')
        print("Current wind speed    :", rows["wind_speed", 'kmph'])


# Create data base to store city weather
def create_db_connection():  # create db connection and return the connection
    try:
        cwdatabase = sqlite3.connect('City_weather.db')  # this creates and name our db
    except Error as e:
        print(e)
    return cwdatabase


# create table
def create_table(db_connection, create_table_sql):
    try:
        cursor = db_connection.cursor()
        cursor.execute(create_table_sql)
    except Error as e:
        print(e)


async def main():
    create_table_sql = ('''
                CREATE TABLE IF NOT EXISTS City_Weather_Data
                ([generate_id] INTEGER PRIMARY KEY,
                [City_Name] TEXT,
                [Cloud] TEXT,
                [tempreture] TEXT,
                [wind_speed] TEXT,
                [humidity] TEXT,
                [country] TEXT)
                ''')
    # create a database connection
    db_connection = create_db_connection()
    # create table
    if db_connection is not None:
        create_table(db_connection, create_table_sql)
    else:
        print("Error while creating database.")

    #data fetching loop
    async with aiohttp.ClientSession() as session:
        await periodic_fetch(db_connection, user_api_key, city_name, session)

        # loop for getting data from database
    while True:
        display_weather(db_connection)
        asyncio.sleep(60)

asyncio.run(main())



